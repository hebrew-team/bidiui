pref("bidiui.mail.compose.default_direction", "ltr");
pref("bidiui.mail.compose.reply_in_default_direction", false);
pref("bidiui.mail.compose.show_direction_buttons", true);
pref("bidiui.mail.compose.space_between_paragraphs.value", "0");
pref("bidiui.mail.compose.space_between_paragraphs.scale", "cm");

// Hidden prefs
pref("bidiui.mail.display.autodetect_direction", true);
pref("bidiui.mail.display.autodetect_bidi_misdecoding", true);
pref("bidiui.mail.compose.alternative_enter_behavior", true);
pref("bidiui.mail.compose.start_composition_in_paragraph_mode", false);
pref("bidiui.mail.display.user_accepts_unusable_charset_pref", false);
pref("bidiui.mail.display.decode_numeric_html_entities", true);
pref("bidiui.mail.compose.last_used_direction", "ltr");
pref("bidiui.mail.compose.ctrl_shift_switches_direction",true);
pref("bidiui.mail.compose.default_to_send_text_with_html",false);
